# Atlantis

[Atlantis](https://www.runatlantis.io/) enables Terraform Pull Request Automation and is deployed on the ```jumpbox``` virtual machine on Flexible Engine for each participating Member State.

This repository contains some sample configurations to setup Atlantis.
